import React, { useEffect, useState } from 'react'
import PlaylistsDetails from '../components/PlaylistsDetails'
import PlaylistsForm from '../components/PlaylistsForm'
import PlaylistsList from '../components/PlaylistsList'
import { Playlist } from '../../model/Playlist'

interface Props {}

const playlistsData: Playlist[] = [{
    id: '123',
    name: 'Playlist 123',
    public: true,
    description: 'Best 123!!! <3'
}, {
    id: '234',
    name: 'Playlist 234',
    public: false,
    description: 'Best 234!!! <3'
}, {
    id: '345',
    name: 'Playlist 345',
    public: true,
    description: 'Best 345!!! <3'
}]

const PlaylistsView = (props: Props) => {
    const [playlists, setPlaylists] = useState<Playlist[]>(playlistsData)
    const [selectedId, setSelectedId] = useState<Playlist['id'] | undefined>()
    const [selectedPlaylist, setSelectedPlaylist] = useState<Playlist | undefined>()

    const [mode, setMode] = useState<'edit' | 'details' | 'create'>('details')
    // const [mode, setMode] = useState<MODES>(MODES.details)

    const selectPlaylist = (id: string | undefined) => { setSelectedId(id); }
    const goToDetails = () => { setMode('details'); }
    const goToEditForm = () => { setMode('edit'); }
    const goToCreateForm = () => {
        setSelectedPlaylist({ id: '', name: '', description: '', public: false })
        setMode('create');
    }

    const savePlaylist = (draft: Playlist) => {
        setPlaylists(prevPlaylists => prevPlaylists.map(p => p.id === draft.id ? draft : p))
        setMode('details');
    }

    const addPlaylist = (draft: Playlist) => {
        draft.id = (Date.now() + Math.ceil(Math.random() * 100_000)).toString()
        setPlaylists(prevPlaylists => [...prevPlaylists, draft])
        setSelectedId(draft.id)
        setMode('details');
    }

    useEffect(() => {
        setSelectedPlaylist(playlists.find(p => p.id === selectedId))
    }, [playlists, selectedId])

    return (
        <div>
            {/* .row>.col*2 */}
            <div className="row">
                <div className="col">
                    <PlaylistsList
                        selectedId={selectedId}
                        playlists={playlists}
                        onSelect={selectPlaylist} />

                    <button className="btn btn-info btn-block float-end mt-3" onClick={goToCreateForm}>
                        Create new playlist
                    </button>
                </div>
                <div className="col">

                    {selectedPlaylist && mode === 'details' && <PlaylistsDetails
                        playlist={selectedPlaylist}
                        onEdit={goToEditForm} />}

                    {selectedPlaylist && mode === 'edit' && <PlaylistsForm
                        playlist={selectedPlaylist}
                        onClickCancel={goToDetails}
                        onSave={savePlaylist} />}

                    {selectedPlaylist && mode === 'create' && <PlaylistsForm
                        playlist={selectedPlaylist}
                        onClickCancel={goToDetails}
                        onSave={addPlaylist} />}

                    {!selectedPlaylist && <p>Please select playlist</p>}

                </div>
            </div>
        </div>
    )
}

export default PlaylistsView
