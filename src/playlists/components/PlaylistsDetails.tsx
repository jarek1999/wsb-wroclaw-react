import React from 'react'
import { Playlist } from '../../model/Playlist'

interface Props {
    playlist: Playlist,
    onEdit: () => void
}

const PlaylistsDetails = ({ playlist, onEdit }: Props) => {

    return (
        <div>
            <dl id="playlist_details" title={playlist.name} data-playlist-id={playlist.id}>
                <dt>Name:</dt>
                <dd className="display-6">{playlist.name}</dd>

                <dt>Public:</dt>
                <dd style={{ color: playlist.public ? 'red' : 'blue' }} >
                    {playlist.public ? 'Yes' : 'No'}
                </dd>

                <dt>Description:</dt>
                <dd>{playlist.description}</dd>
            </dl>

            {/* <button className="btn btn-info" onClick={(e)=>onEdit(e)}>Edit</button> */}
            <button className="btn btn-info" onClick={onEdit}>Edit</button>
        </div>
    )
}

export default PlaylistsDetails
