import React from 'react'
import { SimpleAlbum } from '../../model/Search'

interface Props {
    album: SimpleAlbum
}

const AlbumCard = ({ album }: Props) => {
    return (
        <div className="card">
            <img src={album.images[0].url} className="card-img-top" alt="..." />
            <div className="card-body">
                <h5 className="card-title">
                    {album.name}
                </h5>
            </div>
        </div>
    )
}

export default AlbumCard
