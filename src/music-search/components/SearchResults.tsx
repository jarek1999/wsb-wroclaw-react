import React from 'react'
import { Album } from '../../model/Search'
import AlbumCard from './AlbumCard'

interface Props {
    results: Album[]
}

const SearchResults = ({ results }: Props) => {
    return (
        <div>
            <div className="row row-cols-1 row-cols-sm-4 g-0">
                {results.map(result =>
                    <div className="col" key={result.id}>
                        <AlbumCard album={result} />
                    </div>
                )}
            </div>
        </div>
    )
}

export default SearchResults
