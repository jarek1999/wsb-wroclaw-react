// Generated by https://quicktype.io

import { AxiosResponse } from "axios"

// https://developer.spotify.com/documentation/web-api/reference/#category-search
// GET https://api.spotify.com/v1/search

export interface AlbumSearchResponse {
    albums: PagingObject<Album>
}

export type SearchResponse<T extends { type: string }> = {
    [key in `${T['type']}s`]: PagingObject<T>
}

export interface Album {
    album_type:             string;
    artists:                Artist[];
    available_markets:      string[];
    copyrights:             Copyright[];
    external_ids:           ExternalIDS;
    external_urls:          ExternalUrls;
    genres:                 any[];
    href:                   string;
    id:                     string;
    images:                 Image[];
    name:                   string;
    popularity:             number;
    release_date:           string;
    release_date_precision: string;
    tracks?:                PagingObject<Track>;
    type:                   'album';
    uri:                    string;
}
// export type PartialAlbum = {
//     [key in keyof Album]?: Album[key]
// }

// >> node_modules\typescript\lib\lib.es5.d.ts :

// export type Partial<T> = {
//     [key in keyof T]?: T[key]
// }

// export type Pick<T, K extends keyof T> = {
//     [key in K]: T[key]
// }

export type SimpleAlbum = Pick<Album, 'id' | 'name' | 'type'> & { 
    images: Pick<Album['images'][number],'url'>[]
}

// const a:SimpleAlbum = {
//     type:'album', id:'123', images:[ {url:''} ], name:''
// }


// export interface SimpleAlbum {
//     id:                     string;
//     images:                 Image[];
//     name:                   string;
//     type:                   'album';
// }

export interface Artist {
    external_urls: ExternalUrls;
    href:          string;
    id:            string;
    name:          string;
    type:          'artist';
    uri:           string;
}

export interface ExternalUrls {
    spotify: string;
}

export interface Copyright {
    text: string;
    type: string;
}

export interface ExternalIDS {
    upc: string;
}

export interface Image {
    height: number;
    url:    string;
    width:  number;
}

export interface PagingObject<ItemType> {
    href:     string;
    items:    ItemType[];
    limit:    number;
    next:     null;
    offset:   number;
    previous: null;
    total:    number;
}

export interface Track {
    artists:           Artist[];
    available_markets: string[];
    disc_number:       number;
    duration_ms:       number;
    explicit:          boolean;
    external_urls:     ExternalUrls;
    href:              string;
    id:                string;
    name:              string;
    preview_url:       string;
    track_number:      number;
    type:              'track';
    uri:               string;
}


const mockAlbum = [
    { id: '123', name: 'Album 123', type: 'album', images: [{ url: 'https://www.placecage.com/c/300/300' }] },
    { id: '234', name: 'Album 234', type: 'album', images: [{ url: 'https://www.placecage.com/c/400/400' }] },
    { id: '345', name: 'Album 345', type: 'album', images: [{ url: 'https://www.placecage.com/c/500/500' }] },
    { id: '456', name: 'Album 456', type: 'album', images: [{ url: 'https://www.placecage.com/c/600/600' }] },
] as Album[]


export function isSearchResponse<T extends { type: string }>(type: T['type'], resp: any): resp is AxiosResponse<SearchResponse<T>> {
    return Boolean(resp?.data?.albums?.items instanceof Array)
}

export function validateSearchResponse(response: any) {
    if (!isSearchResponse<Album>('album', response)) {
        throw new Error('Invalid server response')
    }
    return response
}