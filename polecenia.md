
## Git
cd ..
git clone https://bitbucket.org/ev45ive/wsb-wroclaw-react.git  wsb-wroclaw-react
<!-- VSCODE -> FIle -> Open Folder = wsb-wroclaw-react -->
cd wsb-wroclaw-react
npm i 
npm start 

## GIT updates
<!-- Stash your changes -->
git stash -u 
<!-- Fetch latest changes and merge -->
git pull 
<!-- Update deps -->
npm i 

# VS COde
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets
https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

## Chrome
https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi

## nowy projekt
npm i -g create-react-app
cd katalog_gdzie_chcemy_wygenerowac_projekt
create-react-app . --template=typescript

npm start

# Playlists module

mkdir -p src/playlists/containers
mkdir -p src/playlists/components
touch src/playlists/containers/PlaylistsView.tsx
touch src/playlists/components/PlaylistsList.tsx
touch src/playlists/components/PlaylistsDetails.tsx
touch src/playlists/components/PlaylistsForm.tsx

tsrafce - typescript react arrow function component export

## Music Search module

mkdir -p src/music-search/containers
mkdir -p src/music-search/components
touch src/music-search/containers/MusicSearchView.tsx
touch src/music-search/components/SearchForm.tsx
touch src/music-search/components/SearchResults.tsx
touch src/music-search/components/AlbumCard.tsx

## Axios
npm i axios

## OAuth2

npm i react-oauth2-hook immutable react-storage-hook

## Router
npm install react-router-dom @types/react-router-dom

